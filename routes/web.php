<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/','HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function() {
    return view('\adminlte\master');
});

Route::get('/table', function() {
    return view('item\table');
});

Route::get('/data-tables', function() {
    return view('item\data');
});

Route::get('/default', function() {
    return view('item\default');
});

//Route CRUD
Route::get('/','CastController@index');//saya custom default menuju index

Route::get('/cast','CastController@index');

Route::get('/cast/create','CastController@create');

Route::post('/cast','CastController@store');

Route::get('/cast/{id}','CastController@show');

Route::get('/cast/{id}/edit','CastController@edit');

Route::put('/cast/{id}','CastController@update');

Route::delete('/cast/{id}','CastController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
