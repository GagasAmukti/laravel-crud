<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\cast;
class CastController extends Controller
{
    public function create(){
        return view('CRUD.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' =>  'required'
        ]);

        /*<----- eluqent ORM MASS ------> */
        $cast = cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]); 

        /*$cast = new cast;
        $cast->nama = $request["nama"];
        $cast->umur = $request["umur"];
        $cast->bio = $request["bio"];
        $cast->save(); */

        return redirect('/cast')->with('success', 'Post Berhasil Tersimpan');
        
        /*<----- Querry builder biasa ------> */
        /*$query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        */
    }

    public function index(){
        $cast = cast::all(); 
        //$cast = DB::table('cast')->get();
        return view('CRUD.index', compact('cast'));
    }

    public function show($id){
        $cast = cast::find($id);
        //$cast = DB::table('cast')->where('id', $id)->first();
        //dd($cast);
        return view('CRUD.show', compact('cast'));
    }

    public function edit($id){
        $cast = cast::find($id);
        //$cast = DB::table('cast')->where('id', $id)->first();
        return view('CRUD.edit', compact('cast'));
    }

    public function update($id, Request $request){
        
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' =>  'required'
        ]);

        /*<----- eluqent ORM MASS ------> */
        $update = cast::where('id', $id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"]
        ]);

        /*<----- Querry builder biasa ------> */
        /*$query = DB::table('cast')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio'  => $request['bio']
                    ]);*/
        

        return redirect('/cast')->with('success', 'Berhasil Update Data');
    }

    public function destroy($id){
        cast::destroy($id);
        //$query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Post Berhasil Terhapus');
    }
}
