@extends('adminlte.master')

@push('scripts')
@if (session('success'))
<script>
  swal({
    title: "Berhasil Hore",
    text: "{{ session('success') }}",
    icon: "success",
  });
</script>
@endif
@endpush

@section('judul')
<h3 class="card-title">Main Page</h3>
@endsection

@section('content')
<a type="button" class="btn btn-primary" href="/cast/create">Buat Data Baru</a><br><br>
<div class="card">
  <iframe width="800" height="600"  src="https://www.youtube.com/embed/bvltaWCxFl0?list=RDCkU9zS1gLgo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br>
    <div class="card-header">
      <h3 class="card-title">Table Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        
      <table class="table table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($cast as $key => $cas)
          <tr>
            <td> {{ $key + 1 }} </td>
            <td> {{ $cas->nama }} </td>
            <td> {{ $cas->umur }} </td>
            <td> {{ $cas->bio }} </td>
            <td style="display: flex;">  
                <a href="/cast/{{$cas->id}}" class="btn btn-sm btn-info">Detail</a> &nbsp; 
                <a href="/cast/{{$cas->id}}/edit" class="btn btn-sm btn-success">Update</a>&nbsp; 
                
                <form role="form" action="/cast/{{$cas->id}}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="delete" class="btn btn-sm btn-danger">
                </form>
            </td>
          </tr>
          @empty
          <tr>
            <td></td>
            <td></td>
            <td align="center"> Data Kosong </td>
            <td></td>
            <td></td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection